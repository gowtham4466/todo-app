import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import Button from './genericComponents/Button.vue'
import Modal from "./genericComponents/Modal.vue";
import Textinput from "./genericComponents/Textinput.vue";
import todoState from "./store/todoState";
Vue.config.productionTip = false
Vue.use(Vuex)
const store =  new Vuex.Store(todoState())
Vue.component('Button',Button)
Vue.component('Modal',Modal)
Vue.component('Textinput',Textinput)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: store,
  render: h => h(App)
})
