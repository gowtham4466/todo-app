import initialState from "../helper/initials";
export default function todoState() {
    return {
        state: initialState(),
        mutations:{
            addList(state,payLoad){
                state.lists.push(payLoad)
            },
            modifyState(state,payLoad){
                state.lists = payLoad.list
            },
            addCard(state, payLoad){
                state.lists.forEach(l => {
                    console.log(Object.keys(l)[0],payLoad.listkey)
                    if(l.listName === payLoad.listkey){

                        l['cardDetails'].push(payLoad.cardObj)
                    }
                });
            },
            deleteList(state, payLoad){
                state.lists = state.lists.filter((l)=>{
                    return l.listName !== payLoad.listkey
                })
            },
            deleteCard(state, payLoad){
                state.lists.forEach(l => {
                    if(l.listName === payLoad.listkey){
                        l.cardDetails = l.cardDetails.filter(c=>{
                            console.log(c)
                            return c.id !== payLoad.cardid
                        })
                    }
                })
            },
            flagFavourite(state, payLoad){
                state.lists.forEach(l => {
                    if(l.listName === payLoad.listkey){
                        l.cardDetails = l.cardDetails.filter(c=>{
                            if(c.id === payLoad.cardid){
                                c.isFav = !c.isFav
                            }
                            return c
                        })
                    }
                })
            }
        },
        getters:{
            getList(state){
                return state.lists
            }
        },
        actions: {
            addList ({ commit }, payload) {
                console.log(payload)
              commit('addList',payload)
            },
            addCardToList({ commit }, payload) {
                console.log(payload)
                commit('addCard',payload)
            },
            deleteList({commit }, payload) {
                console.log(payload)
                commit('deleteList', payload)
            },
            deleteCard({commit }, payload) {
                console.log(payload)
                commit('deleteCard', payload)
            },
            flagFavourite({commit }, payload) {
                console.log(payload)
                commit('flagFavourite', payload)
            },
            updateList({commit }, payload) {
                commit('modifyState', payload)
            }
          }
    }
}