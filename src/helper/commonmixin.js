export default  {
    methods: {
        /**
         * function to sort array of object based on on key both ascending and descending
         * @param {*what kind of property string or number} type 
         * @param {object key name} keyName 
         * @param {actual array} items 
         * @param {asc or des} kind
         */
        sort(type, keyName, items,kind) {
            console.log('sort calling',type,kind)
            if (type === 'Number') {
                return items.sort(function (a, b) {
                    if(kind === 'asc') {
                        return a[`${keyName}`] - b[`${keyName}`];
                    } else{
                        return b[`${keyName}`] - a[`${keyName}`]
                    }
                });
            } else if(type === 'date'){
                let sorter = (a, b) => {
                    if(kind === 'asc') {
                        return new Date(a[keyName]).getTime() - new Date(b[keyName]).getTime();
                    } else{
                        return new Date(b[keyName]).getTime() - new Date(a[keyName]).getTime();
                    }
                 }
                 console.log(items)
                 return items.sort(sorter);
            } else {
                return items.sort(function (a, b) {
                    var nameA = a[`${keyName}`].toUpperCase(); // ignore upper and lowercase
                    var nameB = b[`${keyName}`].toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return kind === 'asc' ? -1 : 1;
                    }
                    if (nameA > nameB) {
                        return kind === 'asc' ? 1 : -1;
                    }
        
                    // names must be equal
                    return 0;
                })
        
            }
        },
        /**
         * function to set in local storage of browser
         * @param {*how the key should be } itemkey 
         * @param {*vaue of the key} itemvalue 
         */
        setInLocalStorage(itemkey,itemvalue){
            localStorage.setItem(itemkey,  JSON.stringify(itemvalue))
        },
        /**
         * Funciton to keep avoid normal copy
         * @param {*array of obj to clone} arr 
         */
        shallowCopy(arr){
            return arr.map(item =>{
                return {...item}
            })
        }
    }
  };